#include "RobotOperator.h"
#include"Encryption.h"
#include<iostream>
using namespace std;

/*!
	RobotOerator class contructer with parameter n and sn.
	It initialize accesscode and accessstate.
	\param n is name of user.
	\param sn is surname of user.
*/
RobotOperator::RobotOperator(string n = "Unnamed", string sn = "Unnamed"): name(n) , surname(sn)
{
	accessCode = 0;
	accessState = 0;
}

/*!
	RobotOperator class deconstructer with no parameter.
	deconstruct the objects.
*/
RobotOperator :: ~RobotOperator() {}

/*!
	RobotOperator class encryptCode function with parameter code.
	It sends paremeter to encrypt function to encyrpt code.
	\param code is 4 digit integer.
	\return an encrypted integer.
*/
int RobotOperator:: encryptCode(int code)
{
	this->accessCode = encrypto.encrypt(code);
	return accessCode;
}

/*!
	RobotOperator class decryptCode function with parameter code.
	It sends paremeter to decrypt function to decyrpt code.
	\param code is encrypted integer.
	\return an decrypted integer.
*/
int RobotOperator::decryptCode(int code)
{
	unsigned int decryptedcode = encrypto.decrypt(code);
	return decryptedcode;
}

/*!
	RobotOperator class checkaccesscode function with parameter code.
	It checks the code is same with encrypted code.
	\param code is encrypted integer.
	\return wheter the code is correct or not.
*/
bool RobotOperator::checkAccessCode(int code)
{
	if (code == this->accessCode)
	{
		this->accessState = 1;
		return accessState;
	}
	else return false;
}


/*!
	RobotOperator class print function with no parameter.
	It prints the user information.
*/
void RobotOperator::print()
{
	cout << name << "\t" << surname << "\t" << accessState << endl;
}

/*!
	RobotOperator class setcode function with parameter code.
	It sets the encrypted code and reach it.
	\param code is 4 digit integer.
*/
void RobotOperator::setcode(int code)
{
	encryptCode(code);
}

/*!
	RobotOperator class getcode function with no parameter.
	It gives the encrypted code and reach it.
	\return a 4 digit encrypted integer
*/
int RobotOperator::getcode()
{
	return accessCode;
}

/*!
	RobotOperator class getcode function with no parameter.
	It reaches the accesscode and send it to decryptcode function.
	\return a 4 digit decrypted integer
*/
int RobotOperator:: getdecryptedcode()
{
	return decryptCode(accessCode);
}