#include "RobotControl.h"
#include"Pose.h"
#include<iomanip>
#include<iostream>
#include<cmath>

using namespace std;

/*!
	RobotControl class contructer with parameter.
	\param robot is RobotInterface pointer.
*/
RobotControl::RobotControl(RobotInterface* robot)
{
	iRobot = robot;
	rec.setFileName("Path.txt");
	rec.openFile();
}

/*!
	RobotControl class deconstructer with no parameter.
	deconstruct the objects.
*/
RobotControl::~RobotControl(){}

/*!
	RobotControl class turnleft function with no parameter. 
	Makes the robot turn right.
*/
void RobotControl::turnleft()
{
	if (state == 1)
		iRobot->turnLeft();
}

/*!
	RobotControl class turnright function with no parameter.
	Makes the robot turn left.
*/
void RobotControl::turnright()
{
	if (state == 1)
		iRobot->turnRight();

}

/*!
	RobotControl class forward function with parameter speed.
	\param speed is about the movement of robot.
	Makes the robot move forward.
*/
void RobotControl::forward(float speed)
{
	if (state == 1)
		iRobot->forward(speed);

}

/*!
	RobotControl class print function with no parameter.
	Prints the robot's position information.
*/
void RobotControl:: print()
{
	if (state == 1)
		iRobot->print();

}

/*!
	RobotControl class backward function with parameter speed.
	\param speed is about the movement of robot.
	Makes the robot move backward.
*/
void RobotControl :: backward(float speed)
{
	if (state == 1)
		iRobot->backward(speed);

}

/*!
	RobotControl class getpose function with no paremeter.
	Puts the robot's current position information in a pose object.
	\returns a object includes position information.
*/
Pose RobotControl::getPose()
{
	if (state == 1)
		return iRobot->getPose();
}

/*!
	RobotControl class setpose function with paremeter pos.
	\pos is an object of pose class that includes position information.
	Puts the robot's current position information in a pose object.
*/
void RobotControl::setPose(Pose pos)
{
	if (state == 1)
		iRobot->setPose(pos);
}

/*!
	RobotControl class stopturn function with no parameter.
	Makes robot's turning stop.
*/
void RobotControl::stopTurn()
{ 
	if (state == 1)
		iRobot->stopTurn();

}

/*!
	RobotControl class stoprobot function with no parameter.
	Makes robot stop.
*/
void RobotControl::stopMove()
{
	if (state == 1)
		 iRobot->stopMove();
	
}

// to do doxygen

void RobotControl::setRobotOperator(RobotOperator* temp)
{
	robooperator = temp;
}

bool RobotControl::addToPath()
{
	if (state == 1)
	{
		if (iRobot == NULL)
			return false;
		else
		{
			path.addPos(iRobot->getPose());
			return true;
		}
	}
	return false;
}

bool RobotControl::clearPath()
{
	int i = 0;
	if (state == 1)
	{
		while (path.removePos(i))
		{
			i++;
		}
		return true;
	}
	return false;
}

bool RobotControl::recordPathToFile()
{
	int i = 0;
	Pose temp;
	if (state == 1)
	{
		while (i < path.getSize())
		{
			temp = path.getPos(i);
			rec << temp;
			i++;
		}
		return true;
	}
	return false;
}

bool RobotControl::openAccess(int code)
{
	if (robooperator->checkAccessCode(code))
		state = true;
	return state;
}

bool RobotControl::closeAccess(int code)
{
	if (robooperator->checkAccessCode(code))
		state = false;
	return state;
}

void RobotControl::updateSensors(){
	iRobot->updateSensors();
}

void RobotControl::addSensor(RangeSensor* sensor) {
	iRobot->addSensor(sensor);
}