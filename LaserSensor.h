/**
*  @file LaserSensor.h
*  @Author Bora Soylu (152120171043@ogrenci.ogu.edu.tr)
*  @date January, 2021
*  @brief This class is about the robot' laser sensors.
*
*	This class stores the robot's laser sensor datas. 
*   However, I wasn't able to figureout how the getLaserRange works as
*   it returns apparently irrelevant data.
*/

#pragma once
#include "pioneerRobotAPI.h"
#include"RangeSensor.h"
#define laser_ranges_value 181

//! Laser Sensor class.
/*!
	The Laser sensor class has constructor-deconstructor and their member functions that updates,
	gets the current sonar sensors whose index is given,
	gets min and max values in the sonar sensor ranges whose index is given,
	returns the angle value of the sensor whose index is given,
	provides an operator[]. 
*/


class LaserSensor : public RangeSensor
{
public:
	//! A constructor.
	LaserSensor();

	//! A constructor.
	LaserSensor(PioneerRobotAPI*);

	//! A destructor.
	~LaserSensor();

	//! A getRange function.
	float getRange(int index)override;

	//! An updateSensor function.
	void updateSensor(float[]);

	//! An updateSensor function.
	void updateSensor() override;

	//! A getMax function.
	float getMax(int &index)override;

	//! A getMin function.
	float getMin(int &index)override;

	//! A getAngle function.
	float getAngle(int index)override;

	//! A getClosestRange function.
	float getClosestRange(float startAngle, float endAngle, float& angle)override;

	//! An overloaded operator.
	float operator[](int i)override;

private:
	float ranges[laser_ranges_value];
	PioneerRobotAPI* robotAPI; 
};

