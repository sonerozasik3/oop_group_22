#pragma once
/**
*  @file Encryption.h
*  @Author Mete Ozkaya (152120181003@ogrenci.ogu.edu.tr)
*  @date January, 2021
*  @brief This class is about the encyrption.
*
*/

//! A Encryption class.
/*!
	The Encryption class has constructor-deconstructor and their member functions that updates,
	gets a 4 digit integer. it gets 4 digit number and ecrypt it. also get an encrypted 4 digit integer and decrypt it.
	returns encrypted and decrypted integers.
*/

class Encryption
{
public:
	//! A constructor.
	Encryption();
	//! A deconstructor.
	~Encryption();
	//! A encrypt Function.
	int encrypt(int);
	//! A decrypt Function.
	int decrypt(int);
};