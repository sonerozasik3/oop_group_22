/**
*  @file RangeSensor.h
*  @Author Bora Soylu (152120171043@ogrenci.ogu.edu.tr)
*  @date January, 2021
*  @brief This class is abstract class.
*
*	This class is an interface class for to providing new sensor classes.
*/
#pragma once
#include"PioneerRobotAPI.h"
#include<vector>
using namespace std;
//! A RangeSensor class.
/*!
	This class has pure virtual methods that needs to be override.
*/
class RangeSensor {

private:
	float *ranges;
	PioneerRobotAPI* robotAPI;

public:
	//! A getRange pure virtual function.
	virtual float getRange(int index) = 0;

	//! A getMax pure virtual function.
	virtual float getMax(int& index) = 0;

	//! A getMin pure virtual function.
	virtual float getMin(int& index) = 0;

	//! A getAngle pure virtual function.
	virtual float getAngle(int index) = 0;
	//! A getClosestRange pure virtual function.
	virtual float getClosestRange(float, float, float&) = 0;
	
	//! An updateSensor pure virtual function.
	virtual void updateSensor() = 0;

	//! A pure virtual overloaded operator.
	virtual float operator[](int i)=0;
};