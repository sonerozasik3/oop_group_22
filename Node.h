#pragma once
#include"Pose.h"
//! An struct.
/*! Restores the pose nodes and their next pointer */
struct  Node
{
	Node* next;
	Pose pose;
};