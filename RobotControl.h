#pragma once
/**
*  @file RobotControl.h
*  @Author Mete Ozkaya (152120181003@ogrenci.ogu.edu.tr)
*  @date January, 2021
*  @brief This class is about the Controlling the Robot.
*
* This class stores robot's position and make robot move.
*/
#include"RangeSensor.h"
#include<vector>
#include"Pose.h"
#include"PioneerRobotAPI.h"
#include"RobotInterface.h"
#include"Path.h"
#include"RobotOperator.h"
#include"Record.h"
//! A RobotControl class.
/*!
	The RobotControl class has constructor-deconstructor and their member functions that updates position and movement type,
	gets the current posiiton and give it,
	gets move commands and make robot stop, move, turn etc.
*/


class RobotControl
{
private:
	vector <RangeSensor*> rangesens; //??
	RobotInterface* iRobot;
	Record rec;
	RobotOperator * robooperator;
	Path path;
	bool state = 0;
public:
	//! A constructor.
	RobotControl(RobotInterface*);
	//! A deconstructor.
	~RobotControl();
	//! A turnleft Function.
	void turnleft();
	//! A turnright Function.
	void turnright();
	//! A Forward Function.
	void forward(float speed);
	//! A print Function.
	void print();
	//! A Backward Function.
	void backward(float speed);
	//! A getPose Function.
	Pose getPose();
	//! A setPose Function.
	void setPose(Pose);
	//! A stopTurn Function.
	void stopTurn();
	//! A stopMove Function.
	void stopMove();
	//! An addSensor Function.
	void addSensor(RangeSensor* sensor);
	//! An updateSensors Function.
	void updateSensors();
		// to do doxygen
	void setRobotOperator(RobotOperator*);
	bool addToPath();
	bool clearPath();
	bool recordPathToFile();
	bool openAccess(int);
	bool closeAccess(int);
};

