/**
* @file Pose.h
* @Author Soner �za��k (sonerozasik@gmail.com)
* @date January, 2021
* @brief This file is about Pose Class.
*
* Pose Class
*/
#pragma once
#include<iostream>
using namespace std;
//! A Pose class.
/*!
	The pose class has constructor-deconstructor and their member functions that updates,
	gets the pose ,provides the operators.
*/

class Pose
{
public:
	//! A constructor.
	~Pose();

	//! A deconstructor.
	Pose();

	//! An getX method.
	float getX();

	//! An setX method.
	void setX(float);

	//! A getY method.
	float getY();

	//! A setY method.
	void setY(float);

	//! A getTh method.
	float getTh();

	//! A setTh method.
	void setTh(float);

	//!An overloaded operator== .
	bool operator==(const Pose&);

	//!An overloaded operator+ .
	Pose operator+(const Pose&);

	//!An overloaded operator- .
	Pose operator-(const Pose&);

	//!An overloaded operator+= .
	Pose& operator+=(const Pose&);

	//!An overloaded operator-= .
	Pose& operator-=(const Pose&);

	//!An overloaded operator< .
	bool operator<(const Pose&);

	//!A getPose method.
	void getPose(float&, float&, float&);

	//!A setPose method.
	void setPose(float, float, float);

	//!A findDistanceTo method.
	float findDistanceTo(Pose);

	//!A findAngleTo method.
	float findAngleTo(Pose);

	//!A copy constructer.
	Pose(const Pose&);

	//!A print method.
	void print(void);


private:
	float y;
	float x;
	float th;
};

