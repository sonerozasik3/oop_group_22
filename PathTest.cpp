#include"Path.h"

void PathTestAllInOne();
void PathTest1();
void PathTest2();
void PathTest3();
void PathTest4();


void PathTestAllInOne() {
	PathTest1();
	PathTest2();
	PathTest3();
	PathTest4();
}

void PathTest1() {
	try {
		cout << "+----------------------------------+" << endl;
		cout << "            TEST-1(AddPose)" << endl;
		cout << "+----------------------------------+" << endl;
		cout << endl << endl;
		Pose pose1;
		Pose pose2;
		Pose pose3;
		Pose pose4;
		Pose pose5;
		pose1.setPose(1, 1, 1);
		pose2.setPose(2, 2, 2);
		pose3.setPose(3, 3, 3);
		pose4.setPose(4, 4, 4);
		pose5.setPose(5, 5, 5);
		Path path;
		path.addPos(pose1);//Adding pose by using addPos() funciton.
		path.addPos(pose2);
		path.addPos(pose3);
		path.print();//Print by using print() function.
		cout << endl << endl;
		path >> pose4;//Adding pose by using >> operator.
		path >> pose5;
		cout << path;//Print by using << operator.
		cout << endl << endl;
	}
	catch (const std::exception& ex) {
		cout << ex.what() << endl;
	}

}

void PathTest2() {
	try {
		cout << "+----------------------------------+" << endl;
		cout << "         TEST-2(GetPos and [])" << endl;
		cout << "+----------------------------------+" << endl;
		cout << endl << endl;
		Pose pose1;
		Pose pose2;
		Pose pose3;
		pose1.setPose(1, 1, 1);
		pose2.setPose(2, 2, 2);
		pose3.setPose(3, 3, 3);
		Path path;
		Path path_copy;
		Path path_copy2;
		path >> pose1;
		path >> pose2;
		path >> pose3;

		Pose pose1_copy;
		Pose pose2_copy;
		Pose pose3_copy;
		pose1_copy = path.getPos(0);
		pose2_copy = path.getPos(1);
		pose3_copy = path.getPos(2);
		path_copy >> pose1_copy;
		path_copy >> pose2_copy;
		path_copy >> pose3_copy;

		Pose pose1_copy2;
		Pose pose2_copy2;
		Pose pose3_copy2;
		pose1_copy2 = path[0];
		pose2_copy2 = path[1];
		pose3_copy2 = path[2];
		path_copy2 >> pose1_copy2;
		path_copy2 >> pose2_copy2;
		path_copy2 >> pose3_copy2;
		cout << "ORIGINAL PATH : " << endl;
		cout << path << endl << endl;
		cout << "COPY OF PATH WITH-->getPos(): " << endl;
		cout << path_copy << endl << endl;
		cout << "COPY OF PATH WITH-->[] operator: " << endl;
		cout << path_copy2 << endl << endl;
	}
	catch (const std::exception& ex) {
		cout << ex.what() << endl;
	}

}
void PathTest3() {
	try {
		cout << "+----------------------------------+" << endl;
		cout << "            TEST-3(insertPos)" << endl;
		cout << "+----------------------------------+" << endl;
		cout << endl << endl;

		Path path;
		Pose pose1;
		Pose pose2;
		Pose pose3;
		Pose pose4;
		Pose pose5;
		Pose pose6;
		Pose pose7;
		pose1.setPose(1, 1, 1);
		pose2.setPose(2, 2, 2);
		pose3.setPose(3, 3, 3);
		path >> pose1;
		path >> pose2;
		path >> pose3;


		cout << path << endl;
		cout << "Inserting Pose = { X=4 | Y=4 | TH= 4 } at index 2..." << endl << endl << endl;
		pose4.setPose(4, 4, 4);
		path.insertPos(2,pose4);
		cout << path << endl;
		cout << "Inserting Pose = { X=5 | Y=5 | TH= 5 } at index 0..." << endl << endl << endl;
		pose5.setPose(5, 5, 5);
		path.insertPos(0, pose5);
		cout << path << endl;
		cout << "Inserting Pose = { X=6 | Y=6 | TH= 6 } at index 5..." << endl << endl << endl;
		pose6.setPose(6, 6, 6);
		path.insertPos(5, pose6);
		cout << path << endl;
		cout << "Inserting Pose = { X=99 | Y=99 | TH= 99 } at index 99..." << endl << endl << endl;
		pose7.setPose(99, 99, 99);
		path.insertPos(99, pose7);
		cout << path << endl;
		cout << "Inserting Pose = { X=99 | Y=99 | TH= 99 } at index 4..." << endl << endl << endl;
		pose7.setPose(99, 99, 99);
		path.insertPos(4, pose7);
		cout << path << endl;
		
	}
	catch (const std::exception& ex) {
		cout << ex.what() << endl;
	}
}
void PathTest4() {
	try{
		cout << "+----------------------------------+" << endl;
		cout << "            TEST-4(removePos)" << endl;
		cout << "+----------------------------------+" << endl;
		cout << endl << endl;

		Path path;
		Pose pose1;
		Pose pose2;
		Pose pose3;
		Pose pose4;
		Pose pose5;
		pose1.setPose(1, 1, 1);
		pose2.setPose(2, 2, 2);
		pose3.setPose(3, 3, 3);
		pose4.setPose(4, 4, 4);
		pose5.setPose(5, 5, 5);
		path >> pose1;
		path >> pose2;
		path >> pose3;
		path >> pose4;
		path >> pose5;

		cout << path << endl;
		cout << "Path::removePos(2)" << endl << endl << endl;
		path.removePos(2);
		cout << path  << endl;
		cout << "Path::removePos(5)" << endl << endl << endl;
		path.removePos(5);
		cout << path  << endl;
		cout << "Path::removePos(2)" << endl << endl << endl;
		path.removePos(2);
		cout << path  << endl;
		cout << "Path::removePos(0)" << endl << endl << endl;
		path.removePos(0);
		cout << path  << endl;
		cout << "Path::removePos(1)" << endl << endl << endl;
		path.removePos(1);
		cout << path  << endl;
		cout << "Path::removePos(0)" << endl << endl << endl;
		path.removePos(0);
		cout << path << endl << endl << endl << endl;
	}
	catch (const std::exception& ex) {
		cout << ex.what() << endl;
	}
}