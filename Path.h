/**
* @file Pose.h
* @Author Soner �za��k (sonerozasik@gmail.com)
* @date January, 2021
* @brief This file is about Path Class.
*
* Path Class
*/
#pragma once
#include"Pose.h"
#include"Node.h"
//! A Path class.
/*!
	The path class has constructor-deconstructor and their member functions that updates,
	gets the path ,provides the operators.
*/
class Path
{
public:
	//! A constructor.
	Path();

	//! A deconstructor.
	~Path();

	//! A addPos method.
	void addPos(Pose);

	//! A print method.
	void print(void) const;

	//! An overloaded operator.
	Pose operator[](int i);

	//! A getPos method.
	Pose getPos(int);

	//! A removePos method.
	bool removePos(int);

	//! A insertPos method.
	bool insertPos(int, Pose);

	//! An overloaded operator.
	friend ostream& operator<<(ostream& out, const Path& path);

	//! An overloaded operator.
	friend istream& operator>>(Path&, Pose& pose);

	//! An getSize method.
	int getSize();

private:
	Node* head;
	Node* tail;
	int number;
};

