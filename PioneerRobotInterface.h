#pragma once
/**
*  @file RobotControl.h
*  @Author Ozan Ayko (152120181014@ogrenci.ogu.edu.tr)
*  @date January, 2021
*  @brief This class is about the Controlling the Robot.
*
* This class is derived from RobotInterface.
*/
#include"RobotInterface.h"
#include"PioneerRobotAPI.h"
#include"Pose.h"

//! A PioneerRobotInterface class.
/*!
	The PioneerRobotInterface class has constructor-deconstructor and their member functions about movement.
	Gets move commands and make robot stop, move, turn etc.
*/

class PioneerRobotInterface : public RobotInterface {
private:

	PioneerRobotAPI* robotAPI;

public:
	//! A constructor.
	PioneerRobotInterface(PioneerRobotAPI*);
	//! A deconstructor.
	~PioneerRobotInterface();
	//! A turnleft Function.
	void turnLeft();
	//! A turnright Function.
	void turnRight();
	//! A Forward Function.
	void forward(float speed);
	//! A print Function.
	void print();
	//! A forward Function.
	void backward(float speed);
	//! A getPose Function.
	Pose getPose();
	//! A setPose Function.
	void setPose(Pose);
	//! A stopTurn Function.
	void stopTurn();
	//! A stopMove Function.
	void stopMove();
	

};