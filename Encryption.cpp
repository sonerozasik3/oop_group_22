#include "Encryption.h"
#include<iostream>
#include<string>
#include<cmath>
using namespace std;

/*!
	Encryption class contructer with no parameter.
*/
Encryption ::Encryption(){
}

/*!
	Encryption class contructer with no parameter.
	It deconstructs the object.
*/
Encryption :: ~Encryption(){
}

/*!
	Encryption class contructer with parameter code.
	\param code is 4 digit integer.
	\return an encrypted integer.
*/
int Encryption :: encrypt(int code)
{
	int digit1, digit2, digit3, digit4, tempdigit , temp;

	temp = code;
	int count = 0;
	while (temp > 0)
	{
		temp /= 10;
		count++;
	}

	if (count < 4)
	{
		return 0;
	}

	digit4 = code % 10;
	code /= 10;
	digit3 = code % 10;
	code /= 10;
	digit2 = code % 10;
	code /= 10;
	digit1 = code % 10;
	code /= 10;

	digit1 = (digit1 + 7) % 10;
	digit2 = (digit2 + 7) % 10;
	digit3 = (digit3 + 7) % 10;
	digit4 = (digit4 + 7) % 10;

	code = stoi(to_string(digit3) + to_string(digit4) + to_string(digit1) + to_string(digit2));
	return code;
}

/*!
	Encryption class contructer with parameter code.
	\param code is 4 digit encrypted integer.
	\return a decrypted integer.
*/
int Encryption :: decrypt(int code)
{
	int temp = code;
	int digit1 = 0, digit2 = 0, digit3 = 0, digit4 = 0;

	if (digit2 == 0)
		digit2 = temp % 10;
	temp /= 10;
	if (digit1 == 0)
		digit1 = temp % 10;
	temp /= 10;
	if (digit4 == 0)
		digit4 = temp % 10;
	temp /= 10;
	if (digit3 == 0)
		digit3 = temp % 10;
	temp /= 10;

	if (digit1 < 7) {
		digit1 += 10;
		digit1 -= 7;
	}
	else
		digit1 -= 7;

	if (digit2 < 7) {
		digit2 += 10;
		digit2 -= 7;
	}
	else
		digit2 -= 7;

	if (digit3 < 7) {
		digit3 += 10;
		digit3 -= 7;
	}
	else
		digit3 -= 7;

	if (digit4 < 7) {
		digit4 += 10;
		digit4 -= 7;
	}
	else
		digit4 -= 7;

	code = stoi(to_string(digit1) + to_string(digit2) + to_string(digit3) + to_string(digit4));
	return code;
}