#include "PioneerRobotAPI.h"
#include"SonarSensor.h"
#include <iostream>
#include<ctime>
using namespace std;

PioneerRobotAPI* robot;
float sonars[16];

void print(SonarSensor ss) {

	cout << "MyPose is (" << robot->getX() << "," << robot->getY() << "," << robot->getTh() << ")" << endl;
	
	robot->getSonarRange(sonars);
	ss.updateSensor(sonars);

	cout << "Sonar ranges are [ ";
	for (int i = 0; i < 16; i++) 
		cout << ss.getRange(i) << " ";	
	cout << "]" << endl;

	int x = 5, y = 7;

	cout << "Minimum distance value: " << ss.getMin(x) << endl;
	cout << "Maximum distance value: " << ss.getMax(y) << endl;	

	int z = rand() % 7;
	printf("The angle of randomly chosen direction %d btw 0 and 16 is: %.2f\n", z, ss.getAngle(z));

	int a = rand() % 16;
	cout << "The sensor value of randomly chosen " << a << "th index btw 0 and 16 is: " << ss[a] << endl;
}

void Test1();
void Test2();

int main() {
	
	srand(time(0));

	Test1();
	//Test2();
	
	cout << endl << endl;
	return 0;
}

void Test1() {
	cout << "-----------------TEST1-----------------\n\n";
	try {
		srand(time(0)); // for randomly generated the angle of direction

		robot = new PioneerRobotAPI;
		SonarSensor ss(robot);

		if (!robot->connect()) {
			cout << "Could not connect..." << endl;
			return;
		}

		robot->moveRobot(100);
		print(ss);

		robot->turnRobot(PioneerRobotAPI::DIRECTION::left);
		Sleep(1000);
		print(ss);

		robot->turnRobot(PioneerRobotAPI::DIRECTION::forward);
		Sleep(1000);
		print(ss);

		robot->turnRobot(PioneerRobotAPI::DIRECTION::right);
		Sleep(1000);
		print(ss);

		robot->moveRobot(100);
		print(ss);

		robot->turnRobot(PioneerRobotAPI::DIRECTION::left);
		Sleep(1000);
		print(ss);

		robot->turnRobot(PioneerRobotAPI::DIRECTION::forward);
		Sleep(1000);
		print(ss);

		robot->turnRobot(PioneerRobotAPI::DIRECTION::right);
		Sleep(1000);
		print(ss);

		robot->stopRobot();
		robot->setPose(250, 180, 70);
		Sleep(1000);
		print(ss);

		cout << "Press any key to exit...";
		getchar();

		robot->disconnect();
		delete robot;

	}
	catch (const exception& ex) {
		cout << ex.what() << endl;
	}
}

void Test2() {
	cout << "----------------TEST2-----------------\n\n";
	try {
		srand(time(0)); // for randomly generated the sensor value

		
		robot = new PioneerRobotAPI;
		SonarSensor ss(robot);

		if (!robot->connect()) {
			cout << "Could not connect..." << endl;
			return;
		}

		robot->moveRobot(100);
		print(ss);

		robot->turnRobot(PioneerRobotAPI::DIRECTION::left);
		Sleep(1000);
		print(ss);

		robot->turnRobot(PioneerRobotAPI::DIRECTION::forward);
		Sleep(1000);
		print(ss);

		robot->turnRobot(PioneerRobotAPI::DIRECTION::right);
		Sleep(1000);
		print(ss);

		cout << "Press any key to exit...";
		getchar();

		robot->disconnect();
		delete robot;
	}
	catch (const exception& ex) {
		cout << ex.what() << endl;
	}
}