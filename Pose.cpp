#include "Pose.h"
/*!
	Pose class constructor with no parameter.
	Constructs a pose.
*/
Pose::Pose() {
	this->x = 0;
	this->y = 0;
	this->th = 0;
}

/*!
	Pose class deconsturctor with no parameter.
	Deconstructs the pose.
*/
Pose::~Pose() {

}

/*!
	Pose class copy consturctor with no parameter.
	Copies a pose to another.
	@return the copied Pose
*/
Pose::Pose(const Pose& pose) {
	this->x = pose.x;
	this->y = pose.y;
	this->th = pose.th;
}

/*!
	Pose class get method with no parameter.
	@return the x value.
*/
float Pose::getX() {
	return this->x;
}
/*!
	Pose class set method.
	Set the x value with the parameter.
	@param x is the new value of x.
*/
void Pose::setX(float x) {
	this->x = x;
}

/*!
	Pose class get method with no parameter.
	@return the y value.
*/
float Pose::getY() {
	return this->y;
}

/*!
	Pose class set method.
	Set the y value with the parameter.
	@param y is the new value of y.
*/
void Pose::setY(float y) {
	this->y = y;
}

/*!
	Pose class get method with no parameter.
	@return the th value.
*/
float Pose::getTh() {
	return this->th;
}

/*!
	Pose class set method.
	Set the th value with the parameter.
	@param th is the new value of th.
*/
void Pose::setTh(float th) {
	this->th = th;
}

/*!
	Pose class overloaded operator.
	Checks two pose are they equal.
	@return true if they are equal;
*/
bool Pose::operator==(const Pose& pose) {
	if (this->x == pose.x && this->y == pose.y && this->th == pose.th) return true;
	else return false;
}

/*!
	Pose class overloaded operator.
	Add Pose's values to another.
	@return the added Pose
*/
Pose Pose::operator+(const Pose& pose) {
	/*Pose temp;
	temp.setX(this->x + pose.x);
	temp.setY(this->y + pose.y);
	temp.setTh(this->th + pose.th);
	return temp;*/
	this->x += pose.x;
	this->y += pose.y;
	this->th += pose.th;
	return *this;
}

/*!
	Pose class overloaded operator.
	Substracts Pose's value from the another.
	@return the subtracted Pose
*/
Pose Pose::operator-(const Pose& pose) {
	this->x -= pose.x;
	this->y -= pose.y;
	this->th -= pose.th;
	return *this;
}

/*!
	Pose class overloaded operator.
	Add Pose's values to another.
	@return the added Pose
*/
Pose& Pose::operator+=(const Pose& pose) {
	this->x += pose.x;
	this->y += pose.y;
	this->th += pose.th;
	return *this;
}

/*!
	Pose class overloaded operator.
	Substracts Pose's value from the another.
	@return the subtracted Pose
*/
Pose& Pose::operator-=(const Pose& pose) {
	this->x -= pose.x;
	this->y -= pose.y;
	this->th -= pose.th;
	return *this;
}

/*!
	Pose class overloaded operator.
	Checks the parameter pose is greater or not.
	@return true if the second one is the greater
*/
bool Pose::operator<(const Pose& pose) {
	if (this->x + this->y < pose.x + pose.y)return true;
	else return false;
}

/*!
	Pose class get method.
*/
void Pose::getPose(float& x, float& y, float& th) {
	th = this->th;
	x = this->x;
	y = this->y;
}

/*!
	Pose class set method.
*/
void Pose::setPose(float x, float y, float th) {
	this->th = th;
	this->x = x;
	this->y = y;
}

/*!
	Pose class method.
	Calculates distance between two poses.
	@return distance
*/
float Pose::findDistanceTo(Pose pose) {
	double _x = fabs(this->x - pose.getX());
	double _y = fabs(this->y - pose.getY());
	return sqrtf(_x * _x + _y * _y);
}

/*!
	Pose class method.
	Calculates angle between two poses.
	@return angle
*/
float Pose::findAngleTo(Pose pose) {
	return this->th - pose.getTh();
}

/*!
	Pose class print method.
	Prints the values.
*/
void Pose::print(void) {
	cout << "{ X = " << x << " | Y = " << y << " | TH = " << th << " }" << endl;
}
