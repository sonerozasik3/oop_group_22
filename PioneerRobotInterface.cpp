#include"RobotInterface.h"
#include"PioneerRobotAPI.h"
#include"Pose.h"
#include"PioneerRobotInterface.h"
#include <iomanip>
/*!
	PioneerRobotInterface class contructer with parameter.
	\param robot is PioneerRobotAPI pointer.
*/
PioneerRobotInterface::PioneerRobotInterface(PioneerRobotAPI* robot) {
	robotAPI = robot;
}

/*!
	RobotControl class deconstructer with no parameter.
	deconstruct the objects.
*/
PioneerRobotInterface::~PioneerRobotInterface() {

}

/*!
	RobotControl class turnleft function with no parameter.
	Makes the robot turn right.
*/
void PioneerRobotInterface::turnLeft() {

	robotAPI->turnRobot(PioneerRobotAPI::left);
}

/*!
	RobotControl class turnright function with no parameter.
	Makes the robot turn left.
*/
void PioneerRobotInterface::turnRight() {

	robotAPI->turnRobot(PioneerRobotAPI::right);
}


/*!
	RobotControl class print function with no parameter.
	Prints the robot's position information.
*/
void PioneerRobotInterface::print()
{
	cout << setw(25) << "-----My Pose-----" << endl;
	cout << "X Coordinates : " << robotAPI->getX() << endl << "Y Coordinates : " << robotAPI->getY() << endl << "Robot Through : " << robotAPI->getTh() << endl;
}

/*!
	RobotControl class forward function with parameter speed.
	\param speed is about the movement of robot.
	Makes the robot move forward.
*/
void PioneerRobotInterface::forward(float speed)
{
	robotAPI->turnRobot(PioneerRobotAPI::forward);
	robotAPI->moveRobot(speed);
}

/*!
	RobotControl class backward function with parameter speed.
	\param speed is about the movement of robot.
	Makes the robot move backward.
*/
void PioneerRobotInterface::backward(float speed)
{
	robotAPI->moveRobot(speed);
}

Pose PioneerRobotInterface::getPose()
{// to do
	Pose holder;
	holder.setPose(robotAPI->getX(), robotAPI->getY(), robotAPI->getTh());
	return holder;
}

void PioneerRobotInterface::setPose(Pose pos)
{
	robotAPI->setPose(pos.getX(), pos.getY(), pos.getTh());
}

void PioneerRobotInterface::stopTurn()
{ // to do
	robotAPI->turnRobot(PioneerRobotAPI::forward);
}

void PioneerRobotInterface::stopMove()
{
	robotAPI->stopRobot();
}

