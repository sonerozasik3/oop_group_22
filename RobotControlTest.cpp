#include"RobotControl.h"
#include"PioneerRobotAPI.h"
#include"Pose.h"
using namespace std;

PioneerRobotAPI* robotapi;


int main()
{
	Pose pos;
	robotapi = new PioneerRobotAPI;
	RobotControl robot(robotapi);


	if (!robotapi->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}

	//print, get and set function control
	pos.setPose(10, 115, 25);
	robot.setPose(pos);
	robot.print();
	robotapi->setPose(15, 87, 180);
	pos = robot.getPose();
	pos.print();
	robotapi->setPose(0, 0, 0);
	// move functions control
	robot.forward(500);
	Sleep(4500);
	robot.print();

	robot.turnright();
	Sleep(1500);
	robot.stopTurn();

	robot.turnleft();
	Sleep(6500);
	robot.print();
	robot.stopTurn();

	robot.turnright();
	Sleep(6500);
	robot.print();
	robot.stopTurn();

	robot.forward(150);
	Sleep(3500);
	robot.print();

	robot.backward(100);
	robot.print();
	robot.stopMove();

	cout << "Press any key to exit...";
	getchar();

	robotapi->disconnect();
	delete robotapi;
	return 0;
}
