#include"Encryption.h"
#include<iostream>
#include<cstdlib>

using namespace std;

int main()
{
	// Carefull to insert 4 digit number
	Encryption encrypt;
	unsigned int encode , decode;
	encode = encrypt.encrypt(1234);
	if (encode != 189)
		cout << "Hata!" << endl;

	decode = encrypt.decrypt(189);
	if (decode != 1234)
		cout << "Hata!" << endl;

	encode = encrypt.encrypt(0000);
	if (encode != 0)
		cout << "Hata!" << endl;

	decode = encrypt.decrypt(9999);
	if (decode != 2222)
		cout << "Hata!" << endl;

	encode = encrypt.encrypt(00);
	if (encode != 0)
		cout << "Hata!" << endl;

	int deneme = 1357;
	encode = encrypt.encrypt(deneme);
	if (encode != 2480)
		cout << "Hata!" << endl;

	decode = encrypt.decrypt(encode);
	if (decode != deneme)
		cout << "Hata!" << endl;

	encode = encrypt.encrypt(1001);
	if (encode != 7887)
		cout << "Hata!" << endl;

	return 10;
}