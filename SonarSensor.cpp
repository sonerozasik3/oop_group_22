#include "SonarSensor.h"
#include"PioneerRobotAPI.h"
using std::cout;
using std::endl;
using std::exception;

/*!
	Sonar Sensor class with no parameter.
	Allocates the robotAPI attribute.
*/
SonarSensor::SonarSensor() {

	robotAPI = new PioneerRobotAPI;
}

/*!
	Sonar Sensor class with parameter.
	robot is assigned into robotAPI attribute
	\param robot is PioneerRobotAPI pointer.
*/
SonarSensor::SonarSensor(PioneerRobotAPI* robot) {

	this->robotAPI = robot;
}

/*!
	Default destructor for SonarSensor class
*/
SonarSensor::~SonarSensor(){}


/*!
* 
*   updateSensor class updates old sonar ranges with new ones. 
	\param r[] is a float array that takes sonar sensor datas.
	
*/
void SonarSensor::updateSensor(float r[]) {
	for (int i = 0; i < 16; i++)
		ranges[i] = r[i];
}


void SonarSensor::updateSensor() {
	float r[16];
	robotAPI->getSonarRange(r);
	for (int i = 0; i < 16; i++)
		ranges[i] = r[i];
}
/*!
	\param index is a an integer argument that takes given index of the sonar sensor data.
	\return the value of the sensor whose index is given.
*/
float SonarSensor::getRange(int index) {

	if (index < 0 || index > 16)
		throw exception("Error occured::getRange: Out of the index");
		
	return ranges[index];
}

/*!
	\param index is a an integer argument that takes given index of the sonar sensor data.
	\return the angle of the sensor whose index is given.
*/
float SonarSensor::getAngle(int index) { 

	if (index < 0 || index > 7)
		throw exception("Error occured::getAngle: Out of the index");
	
	float angle = -90;

	if (index > 0 && index <= 7) {

		if (index == 7)
			angle = 90;

		angle += 180 / 7 * index;
	}

	return angle;
}

/*!
   \param index is an integer argument that takes index of the sonar sensor data.
   \return the maximum value of the sensor whose index is given.
*/
float SonarSensor::getMax(int& index) {

	int max = ranges[0];
	int hold=0;
	for (int i = 1; i < 16; i++) {
		if (ranges[i] > max) {
			max = ranges[i];
			hold = i;
		}
	}
	index = hold;
	return max;	
}

/*!
   \param index is an integer argument that takes index of the sonar sensor data.
   \return the minimum value of the sensor whose index is given.
*/
float SonarSensor::getMin(int& index) {

	int min = ranges[5];
	int hold=0;
	for (int i = 0; i < 16; i++) {
		if (ranges[i] < min) {
			min = ranges[i];
			hold = i;
		}
	}
	index = hold;
	return min;
}
/*!
	\param &angle is lowest valued value given between startAngle and endAngle
	\return the angle of minimum value between given angles.
*/
float SonarSensor::getClosestRange(float startAngle, float endAngle, float& angle) {
	float swap_angles = startAngle;  //swap angles if startAngle is bigger than endAngle
	if (startAngle > endAngle) {
		startAngle = endAngle;
		endAngle = swap_angles;
	}

	float min = this->ranges[(int)startAngle];  //variable for minimum
	int angle_temp = -1; //variable for holding index
	for (int i = startAngle; i <= endAngle; i++) { //iterate over the ranges array
		if (this->ranges[i] <= min) { //check if current element is smaller 
			angle_temp = i;   //assign angle_temp to current array index
			min = this->ranges[i]; //assign min to current array element
		}
	}
	angle = angle_temp;
	return min;
}

/*!
*  overloaded operator[] explanation for doxygen, do not delete
   \param i is an integer argument that takes index of the sonar sensor data.
   \return the value of the sensor whose index is given.
*/
float SonarSensor::operator[](int i) {
	if (i < 0 || i > 16)
		throw exception("SonarSensor::operator[]: Out of the index");

	return this->ranges[i];
}