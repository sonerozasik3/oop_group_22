#pragma once
/**
*  @file RobotControl.h
*  @Author Soner Ozasik (152120181069@ogrenci.ogu.edu.tr)
*  @date January, 2021
*  @brief This class is an interface class.
*
* This is an abstract interface class that includes virtual methods for new Robot classes.
* This class stores robot's position and make robot move.
*/
#include"Pose.h"
#include<vector>
#include"RangeSensor.h"
//! A RobotInterface class.
/*!
	The RobotInterface class has virtual methods that updates position and movement type,
	gets the current posiiton and give it,
	gets move commands and make robot stop, move, turn etc.
*/
class RobotInterface {

private:

	Pose* position;
	int state;
	vector<RangeSensor*> sensors;
	
public:
	//! A virtual turnLeft Function.
	virtual void turnLeft(){}

	//! A virtual turnRight Function.
	virtual void turnRight() {}

	//! A virtual Forward Function.
	virtual void forward(float  speed) {}

	//! A virtual Backward Function.
	virtual void backward(float  speed) {}

	//! A virtual print Function.
	virtual void print() {}

	//! A virtual getPose Function.
	virtual Pose getPose() { return *position; }

	//! A virtual setPose Function.
	virtual void setPose(Pose pose) {}

	//! A virtual stopTurn Function.
	virtual void stopTurn() {}

	//! A virtual stopMove Function.
	virtual void stopMove() {}

	//! An virtual addSensor Function.
	virtual void addSensor(RangeSensor* sensors_) {
		sensors.push_back(sensors_);
	}

	//! An virtual updateSensors Function
	virtual void updateSensors() {
		for(int i=0;i<sensors.size();i++) sensors[i]->updateSensor();
	}
};