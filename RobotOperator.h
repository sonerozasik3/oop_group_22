#pragma once
/**
*  @file RobotOperator.h
*  @Author Mete Ozkaya (152120181003@ogrenci.ogu.edu.tr)
*  @date January, 2021
*  @brief This class is about the Operate the robot control.
*
*/
#include<string>
#include"Encryption.h"

//! A RobotOperator class.
/*!
	The RobotControl class has constructor-deconstructor and their member functions that updates 4 digit integer,
	gets number and send it to make encrypted or decrypted,
	gets a number and check if its same with encrypted code or not,
	and also it prints user informations.
*/

using namespace std;
class RobotOperator
{
private:
	string name, surname;
	unsigned int accessCode;
	bool accessState;
	Encryption encrypto;
	//! A encryptedCode Function.
	int encryptCode(int);
	//! A decrypted Function.
	int decryptCode(int);
public:
	//! A constructor.
	RobotOperator(string n , string sn);
	//! A destructor.
	~RobotOperator();
	//! A checkAccessCode Function.
	bool checkAccessCode(int);
	//! A print Function.
	void print();
	//! A set Function.
	void setcode(int);
	//! A get Funcction.
	int getcode();
	//! A get Function.
	int getdecryptedcode();
};

