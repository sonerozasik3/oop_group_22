#include "LaserSensor.h"
float k[200];

/*!
	Default constructor for LaserSensor class
	creates a robotAPI object from PioneerRobotAPI
*/
LaserSensor::LaserSensor() {
	for (int i = 0; i < laser_ranges_value; i++)
		this->ranges[i] = -1;
	robotAPI = new PioneerRobotAPI;
}
/*!
	Default destructor for LaserSensor class
*/
LaserSensor::~LaserSensor() {

}
/*!
	Overloaded Default constructor for LaserSensor class
	Takes in a PioneerRobotAPI object pointer and initializes it
	to robot object.
*/
LaserSensor::LaserSensor(PioneerRobotAPI* robot) {
	for (int i = 0; i < laser_ranges_value; i++)
		this->ranges[i] = -1;
	this->robotAPI = robot;

}
/*!
	\param index is a an integer argument that takes given index of the laser sensor data.
	\return the value of the sensor whose index is given.
*/
float LaserSensor::getRange(int index) {
	return this->ranges[index]; 
}
/*!
	\param r[] is a float array that takes laser sensor datas.
	\return the updated sensor datas.
*/
void LaserSensor::updateSensor(float r[]) {
	for (int i = 0; i < laser_ranges_value; i++)
		this->ranges[i] = r[i];
}

void LaserSensor::updateSensor() {
	robotAPI->getLaserRange(k);
	for (int i = 0; i < laser_ranges_value; i++)
		this->ranges[i] = k[i];
}
/*!
   \param index is an integer argument that takes index of the laser sensor data.
   \return the maximum value of the sensor whose index is given.
*/
float LaserSensor::getMax(int& index) {
	float max = this->ranges[0]; //variable for maximum
	int index_temp; //variable for holding index
	for (int i = 0; i < laser_ranges_value; i++) { //iterate over the ranges array
		if (max <= this->ranges[i]) { //check if current element is bigger 
			max = ranges[i]; //assign max to current array element
			index_temp = i; //assign index_temp to current array index
		}
	}
	index = index_temp;  
	return max;
}
/*!
   \param index is an integer argument that takes index of the laser sensor data.
   \return the minimum value of the sensor whose index is given.
*/
float LaserSensor::getMin(int& index) {
	float min = this->ranges[0]; //variable for minimum
	int index_temp;//variable for holding index
	for (int i = 0; i < laser_ranges_value; i++) { //iterate over the ranges array
		if (min >= this->ranges[i]) { //check if current element is smaller 
			min = ranges[i]; //assign min to current array element
			index_temp = i;  //assign index_temp to current array index
		}
	}
	index = index_temp; 
	return min;
}

/*!
	\param index is a an integer argument that takes given index of the laser sensor data.
	\return the angle of the sensor whose index is given.
*/
float LaserSensor::getAngle(int index) { 
	return index;
}
/*!
	\param &angle is lowest valued value given between startAngle and endAngle
	\return the angle of minimum value between given angles.
*/
float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle) { 
	float swap_angles = startAngle;  //swap angles if startAngle is bigger than endAngle
	if (startAngle > endAngle) {
		startAngle = endAngle;
		endAngle = swap_angles;
	}

	float min = this->ranges[(int)startAngle];  //variable for minimum
	int angle_temp = -1; //variable for holding index
	for (int i = startAngle; i <= endAngle; i++) { //iterate over the ranges array
		if (this->ranges[i] <= min) { //check if current element is smaller 
			angle_temp = i;   //assign angle_temp to current array index
			min = this->ranges[i]; //assign min to current array element
		}
	}
	angle = angle_temp; 
	return min;
}

float LaserSensor::operator[](int i) {
	if (i < 0 || i > 181)
		throw exception("LaserSensor::operator[]: Out of the index");

	return this->ranges[i];
}