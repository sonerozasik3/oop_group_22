/**
*  @file SonarSensor.h
*  @Author Ozan Ayko (152120181014@ogrenci.ogu.edu.tr)
*  @date January, 2021
*  @brief This class is about the robot' sonar sensors.
* 
*	This class stores the robot's sonar sensor datas.
*/

#ifndef SONARSENSOR_H
#define SONARSENSOR_H
#include"PioneerRobotAPI.h"
#include"RangeSensor.h"

//! A Sonar Sensor class.
/*! 
	The Sonar sensor class has constructor-deconstructor and their member functions that updates,
	gets the current sonar sensors whose index is given,
	gets min and max values in the sonar sensor ranges whose index is given,
	returns the angle value of the sensor whose index is given,
	provides an operator[]. 
*/

class SonarSensor : public RangeSensor
{
	float ranges[16];
	PioneerRobotAPI* robotAPI;


public:
	//! A constructor with no parameter.
	SonarSensor();

	//! A constructor with parameter.
	SonarSensor(PioneerRobotAPI*);

	//! A destructor.
	~SonarSensor();

	//! An updateSensor function.
	void updateSensor(float[]);

	//! An updateSensor function.
	void updateSensor()override;

	//! A getRange function.
	float getRange(int index)override;	

	//! A getMax function.
	float getMax(int& index)override;

	//! A getMin function.
	float getMin(int& index)override;

	//! A getAngle function.
	float getAngle(int index)override;

	//! A getClosestRange function.
	float getClosestRange(float startAngle, float endAngle, float& angle)override;

	//! An overloaded operator.
	float operator[](int i)override;
};
#endif