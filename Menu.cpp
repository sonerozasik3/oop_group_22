#include "Menu.h"
float a[200], b[200];


Menu::Menu() {
	string name, surname;
	cout << "Please Enter Your Name " << endl;
	cin >> name;
	cout << "Please Enter Your Surname " << endl;
	cin >> surname;
	robotoperator = new RobotOperator(name, surname);
	robot = new PioneerRobotAPI;
	RobotControl* robotcontrol;
	sonar = new SonarSensor(robot);
	laser = new LaserSensor(robot);
	MainPage();
}

void Menu::MainPage() {
	while (1) {
		system("CLS");
		cout << "------------------------------" << endl;
		cout << "          -Main Menu-" << endl;
		cout << "------------------------------" << endl;
		cout << "1. Connection" << endl;
		cout << "2. Motion" << endl;
		cout << "3. Sensor" << endl;
		cout << "4. Encryption" << endl;
		cout << "5. Positions" << endl;
		cout << "6. Quit" << endl;
		cout << "--> ";
		cin >> selection;
		switch (selection)
		{
		case 1:
			Connection();
			break;
		case 2:
			Motion();
			break;
		case 3:
			Sensor();
			break;
		case 4:
			Encryption();
			break;
		case 5:
			Positions();
			break;
		case 6:
			exit(0);
			break;
		default:
			cout << "Entered valid number!" << endl;
			getchar();
			break;
		}
	}
}

void Menu::Connection() {
	while (1) {
		system("CLS");
		cout << "------------------------------" << endl;
		cout << "          -Connection-" << endl;
		cout << "------------------------------" << endl;
		cout << "1. Connect" << endl;
		cout << "2. Disconnect" << endl;
		cout << "3. Go Back" << endl;
		cout << "--> ";
		cin >> selection;

		switch (selection)
		{
		case  1:
			Connect();
			break;
		case 2:
			Disconnect();
			break;
		case 3:
			MainPage();
			break;
		default:
			cout << "Entered valid number!" << endl;
			getchar();
			break;
		}
	}
}

void Menu::Motion() {
	while (1) {
		system("CLS");
		cout << "------------------------------" << endl;
		cout << "          -Motion-" << endl;
		cout << "------------------------------" << endl;
		cout << "1. Move Forward" << endl;
		cout << "2. Move Backward" << endl;
		cout << "3. Turn Left" << endl;
		cout << "4. Turn Right" << endl;
		cout << "5. Stop Turn" << endl;
		cout << "6. Stop Move" << endl;
		cout << "7. Speed Up" << endl;
		cout << "8. Speed Down" << endl;
		cout << "9. Go Back" << endl;
		cout << "--> ";
		cin >> selection;
		switch (selection)
		{
		case 1:
			MoveForward();
			break;
		case 2:
			MoveBackward();
			break;
		case 3:
			TurnLeft();
			break;
		case 4:
			TurnRight();
			break;
		case 5:
			StopTurn();
			break;
		case 6:
			StopMove();
			break;
		case 7:
			SpeedUp();
			break;
		case 8:
			SpeedDown();
			break;
		case 9:
			MainPage();
			break;
		default:
			cout << "Entered valid number!" << endl;
			getchar();
			break;
		}
	}

}
void Menu::Sensor() {
	while (1) {
		system("CLS");
		cout << "------------------------------" << endl;
		cout << "          -Sensor-" << endl;
		cout << "------------------------------" << endl;
		cout << "1. Get All Sensor Data" << endl;
		cout << "2. Get Closest Object" << endl;
		cout << "3. Go Back" << endl;
		cout << "--> ";
		cin >> selection;
		switch (selection)
		{
		case 1:
			GetAllSensorData();
			break;
		case 2:
			GetClosestObject();
			break;
		case 3:
			MainPage();
			break;
		default:
			cout << "Entered valid number!" << endl;
			getchar();
			break;
		}
	}
}

void Menu::Encryption()
{
	while (1) {
		system("CLS");
		cout << "------------------------------" << endl;
		cout << "          -Encryption-" << endl;
		cout << "------------------------------" << endl;
		cout << "1. Encrypt the Code" << endl;
		cout << "2. Decrypt the Code" << endl;
		cout << "3. Open Access" << endl;
		cout << "4. Close Access" << endl;
		cout << "5. Go Back" << endl;
		cout << "--> ";		cin >> selection;
		switch (selection)
		{
		case 1:
			Encrypt();
			break;
		case 2:
			Decrypt();
			break;
		case 3:
			openAccess();
			break;
		case 4:
			closeAccess();
			break;
		case 5:
			MainPage();
			break;
		default:
			cout << "Entered valid number!" << endl;
			getchar();
			break;
		}
	}
}

void Menu::Positions()
{
	while (1) {
		system("CLS");
		cout << "------------------------------" << endl;
		cout << "          -Positions-" << endl;
		cout << "------------------------------" << endl;
		cout << "1. Add Position to Path" << endl;
		cout << "2. Clear All Positions" << endl;
		cout << "3. Record Positions to File" << endl;
		cout << "4. Go Back" << endl;
		cout << "--> ";		cin >> selection;
		switch (selection)
		{
		case 1:
			addPath();
			break;
		case 2:
			clearPath();
			break;
		case 3:
			recPosition();
			break;
		case 4:
			MainPage();
			break;
		default:
			cout << "Entered valid number!" << endl;
			getchar();
			break;
		}
	}
}

void Menu::MoveForward() {
	hiz = 100;
	robotcontrol->forward(hiz);
}
void Menu::MoveBackward() {
	hiz = -100;
	robotcontrol->backward(hiz);
}
void Menu::TurnLeft() {
	robotcontrol->turnleft();
}
void Menu::TurnRight() {
	robotcontrol->turnright();
}
void Menu::StopTurn() {
	robotcontrol->stopTurn();
}
void Menu::StopMove() {
	hiz = 100;
	robotcontrol->stopMove();
}
void Menu::SpeedUp() {
	if (hiz < 0){
	hiz -= 100;
	robotcontrol->backward(hiz);
}
	 else {
	hiz += 100;
	robotcontrol->forward(hiz);
	}
}
void Menu::SpeedDown() {
	if (hiz < 0)
		 {
		hiz += 100;
		robotcontrol->backward(hiz);
		}
	 else if (hiz > 0)
		 {
		hiz -= 100;
		robotcontrol->forward(hiz);
		}
	 else
		return;
}

void Menu::GetAllSensorData() {
	cout << "Laser Sensor --> [ ";
	robotcontrol->updateSensors();
	for (int i = 0; i < 181; i++) {
		cout << laser->getRange(i);
		if (i != 180) {
			cout << " , ";
		}
	}
	cout <<"]"<<endl << endl<<"Sonar Sensor --> [";
	for (int i = 0; i < 16; i++) {
		cout << sonar->getRange(i);
		if (i != 15) {
			cout << " , ";
		}
	}
	cout << "]" << endl << endl;
	system("pause");
}

void Menu::GetClosestObject() {
	sonar->updateSensor(b);
	int index;
	float min;
	min = sonar->getMin(index);
	cout << "Closest object detected by sonar sensor "<<index<<" at " << min << " milimeters." << endl;
	system("pause");
}

void Menu::Connect() {
	robot->connect();
	pioneer = new PioneerRobotInterface(robot);
	robotcontrol=new RobotControl(pioneer);
	robotcontrol->setRobotOperator(robotoperator);
	robotcontrol->addSensor(sonar);
	robotcontrol->addSensor(laser);
}
void Menu::Disconnect() {
	robot->disconnect();
}

void Menu::Encrypt()
{
	unsigned int code;
	cout << "Please Enter The Code" << endl;
	cin >> code;
	robotoperator->setcode(code);
	cout << "The Encrypted Code is : " << robotoperator->getcode()<<endl;
	system("pause");
}

void Menu::Decrypt()
{
	unsigned int decryptedcode;
	decryptedcode = robotoperator->getdecryptedcode();
	cout << "The Decrypted Code is : " << robotoperator->getdecryptedcode() << endl;
	system("pause");
}

void Menu::openAccess()
{
	unsigned int code;
	cout << "Please Insert a Code to Open Robot Control : " << endl;
	cin >> code;
	if (robotcontrol->openAccess(code))
		cout << "You've Reached the Robot Control !!" << endl;
	else
		cout << "You Insert Wrong Password !!" << endl;
	system("pause");
}

void Menu::closeAccess()
{
	unsigned int code;
	cout << "Please Insert a Code to Close Robot Control : " << endl;
	cin >> code;
	if (!robotcontrol->closeAccess(code))
		cout << "You've Closed the Robot Control !!" << endl;
	else
		cout << "You Insert Wrong Password !!" << endl;
	system("pause");
}

void Menu::addPath()
{
	if (robotcontrol->addToPath())
		cout << "You Added Position to Path" << endl;
	else
		cout << "You Didn't Position to Path" << endl;
	system("pause");
}

void Menu::clearPath()
{
	if (robotcontrol->clearPath())
		cout << "You've Cleared All Positions " << endl;
	else
		cout << "You Didn't Clear Positions " << endl;
	system("pause");
}

void Menu::recPosition()
{
	if (robotcontrol->recordPathToFile())
		cout << "You've Recorded Positions to File" << endl;
	else
		cout << "You Didn't Insert Positions" << endl;
	system("pause");
}