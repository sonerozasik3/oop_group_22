#include<iostream>
#include<sstream>
#include<fstream>
#include"Pose.h"
#include"Record.h"
using namespace std;

int main() {

	Pose pose;
	stringstream ssx;
	stringstream ssy;
	stringstream ssTh;
	string xs;
	string ys;
	string ths;

	pose.setPose(10, 20, 30);
	int x = pose.getX();
	int y = pose.getY();
	int th = pose.getTh();

	ssx << x;
	ssx >> xs;

	ssy << y;
	ssy >> ys;

	ssTh << th;
	ssTh >> ths;

	Record record;// ("positions.txt");
	record.setFileName("positions.txt");
	record.openFile();

	Record record2;// ("positions2.txt");
	record2.setFileName("positions2.txt");
	record2.openFile();

	Pose pose2;
	record << pose;
	record2 >> pose2;
	pose2.print();

	record.closeFile();
	cout << endl << endl;
	return 0;
}