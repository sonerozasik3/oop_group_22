#include "Path.h"

/*!
	Path class constructor with no parameter.
	Constructs a path.
*/
Path::Path() {
	this->head = this->tail = NULL;
	this->number = 0;
}

/*!
	Path class deconsturctor with no parameter.
	Deconstructs the path.
*/
Path::~Path() {

}

/*!
	Path class overloaded operator.
	@param index of the pose in the path
	@return pose at the parameter index;
*/
Pose Path::operator[](int index) {
	if (index < 0 || index >= number) throw::exception("Path::operator[] : Out of range");
	return getPos(index);
}

/*!
	Path class overloaded operator.
	Calls print method.
*/
ostream& operator<<(ostream& out, const Path& path) {
	path.print();
	return out;
}

/*!
	Path class overloaded operator.
	Add pose the tail of the path.
*/
istream& operator>>(Path& path, Pose& pose)
{
	istream* in = NULL;
	path.addPos(pose);
	return *in;
}

/*!
	Path class method with a parameter.
	Add pose the tail of the path.
	@param pose is pose which will add
*/
void Path::addPos(Pose pose) {
	Node* node = new Node();
	node->pose = pose;
	if (this->head == NULL) {
		this->head = this->tail = node;
	}
	else {
		this->tail->next = node;
		this->tail = node;
	}
	this->number++;
}

/*!
	Path class method with no parameter.
	Prints the values.
*/
void Path::print()const {
	if (number == 0) throw std::exception("Path::print() : There aren't any pose");
	cout << "-----------------------------------" << endl;
	cout << "Path::print()" << endl;
	cout << "-----------------------------------" << endl;
	Node* temp = this->head;
	for (int i = 0; i < number; i++) {
		cout << "Pose(" << i << ") = ";
		cout << "{ X: " << temp->pose.getX();
		cout << " | Y: " << temp->pose.getY();
		cout << " | TH: " << temp->pose.getTh() << " }" << endl;
		//cout << "-------------------------" << endl;
		temp = temp->next;
	}
}

/*!
	Path class method with no parameter.
	Returns the pose at the parameter index
	@param index is index
*/
Pose Path::getPos(int index) {
	if (index < 0 || index >= this->number) throw std::exception("Path::getPos() : Index out of range");
	Node* temp = head;
	for (int i = 0; i < index; i++) {
		temp = temp->next;
	}
	return temp->pose;
}

/*!
	Path class method with no parameter.
	Removes the pose at the parameter index.
	@param index is index
	@return is succesfully or not
*/
bool Path::removePos(int index) {
	if (index < 0 || index >= this->number) return false;
	if (index == 0) {
		// Case 1: Remove the first node
		this->head = this->head->next;
	}
	else if (index == number - 1) {
		// Case 2: Remove the last node
		Node* temp = head;
		for (int i = 0; i < number - 2; i++) {
			temp = temp->next;
		}
		tail = temp;
		temp->next = NULL;
	}
	else {
		// Case 3: Remove a node between head and tail
		Node* temp = head;
		for (int i = 0; i < index - 1; i++) {
			temp = temp->next;
		}
		temp->next = temp->next->next;
	}
	number--;
	return true;
}

/*!
	Path class method with no parameter.
	Insert a pose at the specific index.
	@param index is index
	@return is succesfully or not
*/
bool Path::insertPos(int index, Pose pose) {
	if (index < 0 || index >= this->number + 1) return false;
	Node* temp = head;
	Node* node = new Node();
	node->pose = pose;
	if (index == number) {
		addPos(pose);
	}
	else if (index == 0 && index != number) {
		node->next = head;
		head = node;
		number++;
	}
	else {
		for (int i = 0; i < index - 1; i++) {
			temp = temp->next;
		}
		node->next = temp->next;
		temp->next = node;
		number++;
	}
}

int Path::getSize()
{
	return number;
}