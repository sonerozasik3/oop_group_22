#include"RobotOperator.h"
#include"Encryption.h"
#include<iostream>

using namespace std;

int main()
{
	RobotOperator robo("mete" , "ozkaya");
	Encryption enc;

	robo.setcode(1324);
	robo.print();

	if(robo.checkAccessCode(4565))
		cout << "Correct!" << endl;
	robo.print();

	if (robo.checkAccessCode(9180))
		cout << "Correct!" << endl;
	robo.print();

	robo.setcode(4652);
	unsigned int temp = robo.getcode();
	cout << temp << endl;

	temp = robo.getdecryptedcode();
	cout << temp << endl;

	robo.print();

	return 0;
}