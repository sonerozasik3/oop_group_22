/**
*  @file Record.h
*  @Author Ozan Ayko (152120181014@ogrenci.ogu.edu.tr)
*  @date January, 2021
*  @brief This class is about the position informations of robot.
*
*    This class stores the position informations of robot into the files.
*
*/

#ifndef RECORD_H
#define RECORD_H
using namespace std;
#include<fstream>
#include"Pose.h"

/**
*   The Record class has constructor and their member functions that opens file,
*   closes file, sets file name, writes line, reads line,
*   overloaded operator<< and operator>>*
*/
class Record
{

	string filename;
	fstream file; 

public:
	//! A constructor.
	Record();

	//! An openFile function.
	bool openFile();

	//! A closeFile function.
	bool closeFile();

	//! A setFileName function.
	void setFileName(string name);

	//! A readLine function.
	string readLine();

	//! A writeLine function.
	bool writeLine(string str);

	//! An overloaded operator<<
	friend Record& operator<<(Record&, Pose&);

	//! An overloaded operator>>
	friend Record& operator>>(Record&, Pose&);
};
#endif 

