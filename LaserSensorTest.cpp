#include "PioneerRobotAPI.h"
#include"LaserSensor.h"
#include <iostream>
using namespace std;

PioneerRobotAPI* robot;
float laserData[laser_ranges_value];

int main() {
	robot = new PioneerRobotAPI;
	LaserSensor ls(robot);

	if (!robot->connect()) {
		cout << "Could not connect..." << endl;
		return 0;
	}

	//getLaserRange acts differently than expected
	robot->getLaserRange(laserData);
	ls.updateSensor(laserData);

	for (int i = 0; i < laser_ranges_value; i++) {
		cout << i << ":  " << ls.getRange(i) << endl;
	}
	robot->disconnect();
	delete robot;
	cout << endl << endl;
}

