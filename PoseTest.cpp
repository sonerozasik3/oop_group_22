#include"Pose.h"
void PoseTestAllInOne();
void PoseTest1();
void PoseTest2();
void PoseTest3();

void PoseTestAllInOne() {
	PoseTest1();
	PoseTest2();
	PoseTest3();
}

void PoseTest1() {
	try {
		cout << "+----------------------------------+" << endl;
		cout << "            TEST-1(Set Get)" << endl;
		cout << "+----------------------------------+" << endl;
		cout << endl << endl;
		cout << "Creating pose"<<endl;
		Pose pose;
		cout << "Pose = ";
		pose.print();


		cout << endl << "setX(5)" << endl;
		pose.setX(5);
		cout << "getX() = " << pose.getX()<<endl;


		cout << endl << "setY(10)" << endl;
		pose.setY(10);
		cout << "getY() = " <<pose.getY()<<endl;
		

		cout << endl << "setTh(15)" << endl;
		pose.setTh(15);
		cout << "getTh() = "<<pose.getTh()<<endl;
		cout << "Pose = ";
		pose.print();


		float x, y, th;
		cout << endl << "setPose(12,24,36)" << endl;
		pose.setPose(12, 24, 36);
		pose.getPose(x, y, th);
		cout << "getPose(x, y, th)" << endl;
		cout << "x = "<<x<<endl<<"y = "<<y<<endl <<"th = "<<th<<endl;
	}
	catch (const std::exception& ex) {
		cout << ex.what() << endl;
	}

}

void PoseTest2() {
	try {
		cout << "+----------------------------------+" << endl;
		cout << "            TEST-2(Oparetors)" << endl;
		cout << "+----------------------------------+" << endl;
		cout << endl << endl;
		Pose pose1;
		Pose pose2;
		Pose pose3;
		pose1.setPose(5, 5, 5);
		pose2.setPose(5, 5, 5);
		pose3.setPose(1, 1, 1);

		cout << "Pose1 = ";
		pose1.print();

		cout << "Pose2 = ";
		pose2.print();

		cout << "Pose3 = ";
		pose3.print();
		cout << endl;

		cout << "Pose1 and Pose2 ";
		if (pose1 == pose2) cout << "are equal." << endl<<endl;
		else cout << " are not equal." << endl << endl;

		cout << "Pose1 and Pose3 ";
		if (pose1 == pose3) cout << "are equal." << endl << endl;
		else cout << " are not equal." << endl << endl ;

		if (pose1 < pose3) cout << "Pose3's values is bigger than Pose1's values"<<endl;
		else cout << "Pose1's values is bigger than or equal Pose3's values" << endl;
		cout << "+----------------------------------+" << endl;

		pose1.setPose(3, 7, 12);
		pose2.setPose(8, 5, 8);

		cout << "Pose1 = ";
		pose1.print();

		cout << "Pose2 = ";
		pose2.print();
		cout << endl;

		cout << "Pose3 = Pose1+Pose2"<<endl << endl;
		pose3 = pose1 + pose2;
		cout << "Pose3 = ";
		pose3.print();
		cout << "+----------------------------------+" << endl;


		pose1.setPose(13, 17, 12);
		pose2.setPose(8, 5, 8);

		cout << "Pose1 = ";
		pose1.print();

		cout << "Pose2 = ";
		pose2.print();
		cout << endl;

		cout << "Pose3 = Pose1-Pose2" << endl << endl;
		pose3 = pose1 - pose2;
		cout << "Pose3 = ";
		pose3.print();
		cout << "+----------------------------------+" << endl;

		pose1.setPose(3, 7, 2);
		pose2.setPose(9, 5, 8);

		cout << "Pose1 = ";
		pose1.print();

		cout << "Pose2 = ";
		pose2.print();
		cout << endl;

		cout << "Pose2 += Pose1" << endl << endl;
		pose2 += pose1 ;
		cout << "Pose2 = ";
		pose2.print();
		cout << "+----------------------------------+" << endl;
		pose1.setPose(3, 5, 2);
		pose2.setPose(9, 7, 8);

		cout << "Pose1 = ";
		pose1.print();

		cout << "Pose2 = ";
		pose2.print();
		cout << endl;

		cout << "Pose2 -= Pose1" << endl << endl;
		pose2 -= pose1;
		cout << "Pose2 = ";
		pose2.print();
		cout << "+----------------------------------+" << endl;
	}
	catch (const std::exception& ex) {
		cout << ex.what() << endl;
	}
}

void PoseTest3(){
	try {
		cout << "+----------------------------------+" << endl;
		cout << "TEST-3(findDistanceTo & findAngleTo)" << endl;
		cout << "+----------------------------------+" << endl;
		cout << endl << endl;

		Pose pose1;
		Pose pose2;
		pose1.setPose(3, 7, 45);
		pose2.setPose(8, 5, 135);

		cout << "Pose1 = ";
		pose1.print();

		cout << "Pose2 = ";
		pose2.print();
		cout << endl;

		cout << "Distance between Pose1 and Pose2 = " << pose1.findDistanceTo(pose2)<<endl;
		cout << "Angle between Pose1 and Pose2 = " << pose1.findAngleTo(pose2)<<endl;

		cout << endl << endl << endl;
	}
	catch (const std::exception& ex) {
		cout << ex.what() << endl;
	}
}
