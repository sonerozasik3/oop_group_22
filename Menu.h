#pragma once
#include<iostream>
#include"RobotControl.h"
#include"PioneerRobotAPI.h"
#include"SonarSensor.h"
#include"LaserSensor.h"
#include"RobotInterface.h"
#include"PioneerRobotInterface.h"
using namespace std;

class Menu
{
public:
	Menu();
	void MainPage();
	void Connection();
	void Motion();
	void Sensor();
	void Encryption();
	void Positions();

	void Encrypt();
	void Decrypt();
	void openAccess();
	void closeAccess();

	void addPath();
	void clearPath();
	void recPosition();

	void Connect();
	void Disconnect();
	
	void MoveForward();
	void MoveBackward();
	void TurnLeft();
	void TurnRight();
	void StopTurn();
	void StopMove();
	void SpeedUp();
	void SpeedDown();

	void GetAllSensorData();
	void GetClosestObject();
private:

	int selection;
	float hiz=0;
	PioneerRobotAPI* robot;
	RobotControl* robotcontrol;
	SonarSensor* sonar;
	LaserSensor* laser;
	RobotInterface iRobot;
	PioneerRobotInterface* pioneer;
	RobotOperator* robotoperator;
};

