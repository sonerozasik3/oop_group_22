#include<iostream>
#include "Record.h"
#include <string.h>
#include<sstream>
#include"Pose.h"
using namespace std;

/*!
	Record class with no parameter.
	
*/
Record::Record() {
	
}


/*!
*
*   openFile member function that opens file
*/
bool Record::openFile() {

	file.open(filename, ios::out | ios::in);

	if (!file.is_open())
	{
		file.open(filename, ios::out);
	}

	if (!file.is_open()) {
		cout << "File is not opened" << endl;
		return false;
	}
	
	return true;
}

/*!
*
*   closeFile member function that closes file
*/
bool Record::closeFile() {

	file.close();
	cout << "The file is closed successfully" << endl;
	return true;
}

/*!
*
*   setFileName member function that sets file name
*/
void Record::setFileName(string name) { filename = name; }


/*!
*
*   readLine member function that reads a file by file.
*/
string Record::readLine() {
	string str;
	getline(file, str);

	return str;
}

/*!
*
*   writeL�ne member function that write a file into file.
*/
bool Record::writeLine(string str) {

	file << str << "\t\t";
	return true;
}

/*!
*
*   overloaded operator>>
*/
Record& operator>>(Record& record, Pose& pos) {
	string s;
	stringstream ss;
	s = record.readLine();
	ss << s;
	float x, y, th;
	ss >> x;
	ss >> y;
	ss >> th;
	pos.setPose(x, y, th);
	return record;
}

/*!
*
*   overloaded operator<<
*/
Record& operator<<(Record& record, Pose& pos) {
	record.file << "    " << pos.getX() << "\t\t" << pos.getY() << "\t\t" << pos.getTh() << endl;
	return record;
}
